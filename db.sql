create database fashion;
use fashion;

create table product(product_id char(4) Primary key, pname varchar(20) not null, brand varchar(10), product_for varchar(6), season varchar(6), rate int(5) not null);
create table purchase(purchase_id char(20) not null, item_id char(4) references product(product_id), no_of_items int(3) not null, amount int(7), purchase_date date);
create table stock(item_id char(20) references product(product_id), instock int(3) not null, status varchar(10) not null);
create table sales(sale_id char(20) Primary key, item_id char(4) references product(product_id), no_of_items_sold int(3) not null, sale_rate int(5) not null, amount int(7) not null, date_of_sale date);