FROM mysql:latest
WORKDIR /app
RUN apt update && apt install -y python3-pip
RUN apt-get install -y mysql-client
RUN apt-get install -y libmysqlclient-dev
RUN apt install -y python3-dev
RUN apt-get install -y libssl-dev
RUN pip3 install mysql-connector-python
ADD code.py /app
ADD db.sql /app